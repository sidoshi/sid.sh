---
title: Host Private NPM Packages For Free
date: "2018-03-21"
tags: ['javascript', 'yarn', 'npm', 'package', 'git-ssh-env', 'ssh']
---

Using npm packages is a great way to write modular JavaScript code. But as you
may already know, it costs [money](https://www.npmjs.com/pricing) to host
private packages on npm.

But don’t worry. There is a way through which we can get the benefits of using
npm private packages for free. As `npm` and `yarn` both have support for using
git repos as packages, we can use that to host our private packages for free.
Both [BitBucket](http://bitbucket.org/) and [Gitlab](http://gitlab.com/) allows
free private repositories. You may still need to pay if you want to use
[GitHub](http://github.com/).

To use this, inside `package.json` where we put our dependencies, put the git
url of the private repo instead of a version.

Example package.json:

```json{5}
{
  "name": "test-private",
  "version": "1.0.0",
  "dependencies": {
    "private-package": "git+ssh://git@gitlab.com/sidoshi/private-package.git"
  }
}
```

This is the format you need to use:

```bash
git+ssh://git@<host>/<user>/<repo>.git[#branch|tag|hash]
```

Still there is one important problem we are facing. Private repos can only be
accessed with proper ssh keys configured. Now this won’t be an issue in your
local machine assuming you already have your ssh keys in place. But in CI
builds, we need a way to setup these keys automatically and cleanup later when
we are done installing packages. I have built
[git-ssh-key](https://npmjs.com/package/git-ssh-key) to solve this issue.
It helps in setting up ssh keys in CI builds.

### How to use it?

First of all you need to encode the private ssh key you use for accessing your
repo using `base64`. Then you need to make the encoded string available to your
CI through an environment variable.

`git-ssh-key` reads from the following environment variables:

* `GIT_SSH_KEY_GITHUB`
* `GIT_SSH_KEY_GITLAB`
* `GIT_SSH_KEY_BITBUCKET`

You can add one or more private keys depending on where you are hosting your
repositories.

Then before installing packages, you just have to run

```bash
git-ssh-key setup
```

and
all the appropriate keys are added to your ssh config.

After installation is complete, you can cleanup keys by running

```bash
git-ssh-key teardown
```

Note that `git-ssh-key` only clears keys that it configured itself. It does not
touch anything else that was pre-existing.
