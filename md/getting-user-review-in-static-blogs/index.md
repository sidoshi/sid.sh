---
title: Getting user reviews in static blogs
date: "2018-05-06"
tags: ['javascript', 'google-analytics', 'static-blog', 'review']
---

It is important for any content creator to know what the consumer thinks about
the content. I wanted a way to get reviews for my static blog without developing
and managing my own Service, Datastore and UI. I used [Google Analytics](https://analytics.google.com/) to solve
this problem.

Google Analytics allows you to programmatically send various kinds of events
from your site to their service. It then shows those events in an organised
manner in the Analytics Dashboard. We can use this readily available _free_
service to store our reviews. You can see a live demo at the bottom of this page.

## How it works?

First you need to include the Google Analytics script to your site if you have
not already done that.

GA events include three pieces of data: `category`, `action` and
`label`. Using that we can create a function that takes a review and a label to
create an event and then sends it to GA. The review can be in any form of string.
I prefer 👍 and 👎 but you can also use numbers (1-5) to get detailed ratings.

```js
const sendReview = (review, label) => {
  if (typeof ga === `function`) {
    window.ga(`send`, `event`, {
      eventCategory: `Review`,
      eventAction: review,
      eventLabel: label,
    })
  }
}
```

Then you can call this function from the blog pages on receiving user inputs
providing the title as label.

```js
sendReview('👍', 'The title of your blog')
```

## Looking at the reviews

GA doesn't update the events instantly so it might take some time for the data
to update. Though, you can verify that everything works by opening the
`REAL-TIME > Events` tab to see the events. But if you want to see the events in
organised way, ordered by posts, you have to wait for it to update in Behaviour tab.

To see the reviews organised by posts, open `BEHAVIOR > Events > Top Events`.
Then, click on `Review` to filter events to show only review events. Then, select
`Event Label` as the primary dimension. It will order Review events by post label.
Then add `Event Action` as a secondary dimension. That will show the review
next to each post. By looking at `Total Events`, we can find out how much reviews
each post got.

<img class="gif" src="events-view.gif">

I am using the same mechanism on my blog to get Reviews. You can see the code
for it [here](https://gitlab.com/sidoshi/sid.sh/blob/0dfc27987933b3b18c14c0188ebd63baa1e154f9/src/components/Review.js).
