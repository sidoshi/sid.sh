import React from 'react'
import styled from 'react-emotion'

import HeaderAndMetaTags from '../components/HeaderAndMetaTags'
import WidthWrapper from '../components/WidthWrapper'
import BlogHead from '../components/BlogHead'
import Review from '../components/Review'

const Blog = styled.div`
  padding-top: 30px;

  .blog-head {
    font-size: 2.5rem;
  }
`

export default ({ data }) => {
  const post = data.markdownRemark

  return (
    <WidthWrapper>
      <HeaderAndMetaTags
        title={post.frontmatter.title}
        description={post.excerpt}
        url={'https://sid.sh/blog' + post.fields.slug}
        isBlogPost={true}
      />
      <Blog>
        <BlogHead
          title={post.frontmatter.title}
          date={post.frontmatter.date}
          time={post.timeToRead}
        />

        <div
          className="content"
          dangerouslySetInnerHTML={{ __html: post.html }}
        />
      </Blog>
      <hr />
      <Review label={post.frontmatter.title} />
    </WidthWrapper>
  )
}

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        date(formatString: "DD MMMM, YYYY")
      }
      fields {
        slug
      }
      excerpt
      timeToRead
    }
  }
`
