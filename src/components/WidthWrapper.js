import React from 'react'
import styled from 'react-emotion'

export default styled.div`
  max-width: 700px;
  margin: ${p => (p.marginTop === undefined ? 0 : p.marginTop)} auto
    ${p => (p.marginBottom === undefined ? 0 : p.marginBottom)} auto;
`
