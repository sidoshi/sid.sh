import React from 'react'
import styled from 'react-emotion'
import Link from 'gatsby-link'

import BlogHead from './BlogHead'

const Wrapper = styled.div`
  cursor: pointer;

  &:hover h3 {
    color: ${p => p.theme.secondary};
  }

  a.blog-info {
    color: ${p => p.theme.primaryText};

    &:hover {
      text-decoration: none;
    }
  }
`

export default ({ title, url, date, time, excerpt }) => (
  <Wrapper>
    <Link to={url} className="blog-info">
      <BlogHead title={title} date={date} time={time} />

      <p>{excerpt}</p>
    </Link>
  </Wrapper>
)
