import React from 'react'
import styled from 'react-emotion'
import Link from 'gatsby-link'
import RightArrow from 'react-icons/lib/fa/long-arrow-right'

import WidthWrapper from './WidthWrapper'
import BlogsList from './BlogsList'

const Wrapper = styled.div`
  overflow: auto;
`

export default ({ blogs = [] }) =>
  blogs.length > 0 ? (
    <WidthWrapper marginTop="50px" marginBottom="50px">
      <Wrapper>
        <h2>Recent Blogs</h2>

        <BlogsList blogs={blogs} />

        <Link style={{ float: 'right' }} className="more" to="/blog">
          See More <RightArrow size={10} />
        </Link>
      </Wrapper>
    </WidthWrapper>
  ) : null
