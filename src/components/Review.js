import React from 'react'
import Link from 'gatsby-link'

import ThumbsUp from 'react-icons/lib/fa/thumbs-up'
import ThumbsDown from 'react-icons/lib/fa/thumbs-down'

const Question = ({ respond }) => (
  <p>
    Was this page helpful to you?{' '}
    <span onClick={() => respond(true)} style={{ cursor: 'pointer' }}>
      <ThumbsUp />
    </span>{' '}
    <span onClick={() => respond(false)} style={{ cursor: 'pointer' }}>
      <ThumbsDown />
    </span>
  </p>
)

const ResponseYes = () => <p>Thank you for your response.</p>

const ResponseNo = () => (
  <p>
    Thank you for your response. If you'd like to share what you felt can be
    improved, please <Link to="/about#contact">contact me.</Link>
  </p>
)

export default class extends React.Component {
  state = {
    response: undefined,
  }
  respond = response => {
    if (typeof ga === `function`) {
      window.ga(`send`, `event`, {
        eventCategory: `Review`,
        eventAction: response ? `👍` : `👎`,
        eventLabel: this.props.label,
      })
    }
    this.setState({
      response,
    })
  }
  render() {
    return this.state.response === undefined ? (
      <Question respond={this.respond} />
    ) : this.state.response ? (
      <ResponseYes />
    ) : (
      <ResponseNo />
    )
  }
}
