import React from 'react'
import Link from 'gatsby-link'
import { OutboundLink } from 'gatsby-plugin-google-analytics'
import styled from 'react-emotion'

const Nav = styled.nav`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  background-color: ${p => p.theme.background};
  padding: 10px;
  border-bottom: 1px solid #ddd;

  display: flex;
  justify-content: center;
  align-items: center;

  z-index: 10;

  a {
    margin: 0 4px;
    font-family: Playfair Display, serif;

    @media screen and (min-width: 360px) {
      margin: 0 10px;
    }
  }
`

const Header = () => (
  <Nav>
    <Link to="/" activeClassName="active" exact>
      Home
    </Link>
    <Link to="/about" activeClassName="active">
      About & Contact
    </Link>
    <OutboundLink href="https://go.sid.sh/resume">Resume</OutboundLink>
    <Link to="/blog" activeClassName="active">
      Blog
    </Link>
  </Nav>
)

export default Header
