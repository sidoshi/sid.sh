import React from 'react'
import Link from 'gatsby-link'
import styled from 'react-emotion'
import profile from '../profile'
import { OutboundLink } from 'gatsby-plugin-google-analytics'

const Footer = styled.footer`
  a {
    color: ${props => props.theme.secondaryText};

    &:hover {
      border: none;
      color: ${props => props.theme.secondary};
    }
  }

  p {
    text-align: center;
    margin: 0;
  }

  div {
    max-width: max-content;
    margin: 0 auto;

    span {
      margin: 0 10px;
    }
  }

  border-top: 1px solid ${props => props.theme.secondaryText};
  padding-top: 30px;
  margin-top: 30px;
  margin-bottom: 30px;
`

export default () => (
  <Footer>
    <p>
      <OutboundLink href="https://keybase.io/sidoshi">
        {profile.name}
      </OutboundLink>
    </p>

    <div>
      {profile.platforms.map(({ url, component, title }) => (
        <span key={url}>
          <OutboundLink key={url} href={url} title={title}>
            {component}
          </OutboundLink>
        </span>
      ))}
    </div>
  </Footer>
)
