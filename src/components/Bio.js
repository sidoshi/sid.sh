import React from 'react'
import styled from 'react-emotion'
import profile from '../profile'

const Wrapper = styled.div`
  text-align: center;
`

const Name = styled.h1`
  font-size: 60px;
  margin-top: 100px;
`

const Bio = styled.p`
  max-width: 500px;
  margin-left: auto;
  margin-right: auto;
`

export default () => (
  <Wrapper>
    <Name>{profile.name}</Name>
    <Bio>
      I am a proficient Software Developer with strong affection towards
      JavaScript. I build powerful and maintainable Backend Services and Browser
      applications.
    </Bio>
  </Wrapper>
)
