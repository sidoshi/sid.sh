import React from 'react'
import Helmet from 'react-helmet'

import profile from '../profile'

export default ({
  title = profile.name,
  description = profile.bio,
  url = profile.url,
  image,
  isBlogPost = false,
}) => (
  <Helmet titleTemplate="%s - Siddharth Doshi" defaultTitle="Siddharth Doshi">
    <html lang="en" />
    <title lang="en">{title}</title>
    <meta name="description" content={description} />
    {image ? <meta name="image" content={image} /> : null}
    {/* OpenGraph tags */}
    <meta property="og:url" content={url} />
    {isBlogPost ? <meta property="og:type" content="article" /> : null}
    <meta property="og:title" content={title} />
    <meta property="og:description" content={description} />
    {image ? <meta property="og:image" content={image} /> : null}
    {/* Twitter Card tags */}
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:creator" content="@_sidoshi" />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={description} />
    {image ? <meta name="twitter:image" content={image} /> : null}
    <link rel="canonical" href={url} />
  </Helmet>
)
