import React from 'react'
import styled from 'react-emotion'
import { OutboundLink } from 'gatsby-plugin-google-analytics'
import RightArrow from 'react-icons/lib/fa/long-arrow-right'

import WidthWrapper from './WidthWrapper'

const Wrapper = styled.div`
  overflow: auto;
  p {
    overflow: auto;
  }

  .more {
    float: right;
  }
`

const Tag = styled.span`
  color: ${p => p.theme.secondaryText};
  font-size: 15px;
`

const Circle = styled.span`
  height: 10px;
  width: 10px;
  background-color: ${p => p.color};
  border-radius: 50%;
  display: inline-block;
`

const Project = ({ name, url, description, primaryLanguage }) => (
  <p>
    <OutboundLink href={url}>{name}</OutboundLink>
    {' - '}
    {description}

    <br />
    {primaryLanguage && (
      <span>
        <Circle color={primaryLanguage.color} />{' '}
        <Tag>{primaryLanguage.name}</Tag>
      </span>
    )}
  </p>
)

export default ({ projects = [] }) =>
  projects.length > 0 ? (
    <WidthWrapper marginBottom="50px">
      <Wrapper>
        <h2>Recent Works</h2>
        {projects.map(({ name, url, description, primaryLanguage }) => (
          <Project
            key={url}
            name={name}
            url={url}
            description={description}
            primaryLanguage={primaryLanguage}
          />
        ))}
        <OutboundLink
          className="more"
          href="https://github.com/sidoshi?tab=repositories&q=&type=source"
        >
          See More <RightArrow size={10} />
        </OutboundLink>
      </Wrapper>
    </WidthWrapper>
  ) : null
