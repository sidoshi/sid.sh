import React from 'react'
import styled from 'react-emotion'

import BlogInfo from './BlogInfo'

const Wrapper = styled.div``

export default ({ blogs = [] }) => (
  <Wrapper>
    {blogs.map(({ title, url, date, time, excerpt }) => (
      <BlogInfo
        key={url}
        title={title}
        url={url}
        date={date}
        time={time}
        excerpt={excerpt}
      />
    ))}
  </Wrapper>
)
