import React from 'react'
import styled from 'react-emotion'

const Wrapper = styled.h3`
  margin: 10px 0;

  span.date {
    color: ${p => p.theme.secondaryText};
    font-weight: 400;
    font-size: 15px;
  }
`

const Dot = styled.span`
  &::after {
    content: '\00B7';
  }
`

export default ({ title, date, time }) => (
  <Wrapper className="blog-head">
    {title}
    <br />
    <span className="date">
      {date} <Dot /> {time} min read
    </span>
  </Wrapper>
)
