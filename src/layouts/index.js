import 'prismjs/themes/prism-tomorrow.css'
import '../utils/globalStyles'

import React from 'react'
import Helmet from 'react-helmet'
import styled from 'react-emotion'
import { ThemeProvider } from 'emotion-theming'

import theme from '../utils/theme'
import Header from '../components/Header'
import Footer from '../components/Footer'
import profile from '../profile'

const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 0px 1.0875rem;
  padding-top: 0;
`

const Content = styled.div`
  position: relative;
  margin-top: 70px;
`

const Layout = ({ children }) => {
  const { name: title, bio: description } = profile

  return (
    <div>
      <Helmet
        title={title}
        meta={[{ name: 'description', content: description }]}
      />

      <ThemeProvider theme={theme}>
        <Wrapper>
          <Header />
          <Content>{children()}</Content>
          <Footer />
        </Wrapper>
      </ThemeProvider>
    </div>
  )
}

export default Layout
