import { injectGlobal } from 'react-emotion'
import theme from './theme'

injectGlobal`
.gatsby-highlight-code-line {
  background-color: #14161a;
  display: block;
  margin-right: -1em;
  margin-left: -1.2em;
  padding-right: 2em;
  padding-left: 1em;
  border-left: 0.25em solid ${theme.primary};
}

.gatsby-highlight {
  background-color: #282c34;
  margin: 0.5em 0;
  padding: 1em;
  overflow: auto;
  border-radius: 10px;
}

.gatsby-highlight pre[class*='language-'] {
  background-color: transparent;
  margin: 0;
  padding: 0;
  overflow: initial;
  float: left;
  min-width: 100%;
}

:not(pre) > code[class*='language-'] {
  background-color: #ddd !important;
  color: ${theme.secondary} !important;
}

.anchor {
  &:hover {
    border-bottom: none;
  }
}

`
