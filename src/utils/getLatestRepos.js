const getMockData = () =>
  new Promise(res =>
    setTimeout(
      () =>
        res([
          {
            name: 'decade-out',
            description: 'Set huge timeouts',
            url: 'https://github.com/sidoshi/decade-out',
            primaryLanguage: { name: 'JavaScript', color: '#f1e05a' },
            stargazers: 1,
            forks: 0,
          },
          {
            name: 'run-by',
            description: 'Figure out if the script is run by npm or yarn',
            url: 'https://github.com/sidoshi/run-by',
            primaryLanguage: { name: 'JavaScript', color: '#f1e05a' },
            stargazers: 1,
            forks: 0,
          },
          {
            name: 'git-ssh-key',
            description:
              'Setup ssh private keys for git from environment variables',
            url: 'https://github.com/sidoshi/git-ssh-key',
            primaryLanguage: { name: 'JavaScript', color: '#f1e05a' },
            stargazers: 3,
            forks: 0,
          },
          {
            name: 'profile',
            description: "Siddharth Doshi's Public Profile",
            url: 'https://github.com/sidoshi/profile',
            primaryLanguage: { name: 'JavaScript', color: '#f1e05a' },
            stargazers: 1,
            forks: 0,
          },
        ]),
      '1000'
    )
  )

export default () =>
  new Promise(res => {
    if (typeof location === 'undefined' || typeof fetch === 'undefined') return
    const isDev = location.hostname.includes('sid.sh')
    const url = 'https://sidoshi-latest-repos.now.sh'

    isDev
      ? fetch(url)
          .then(data => data.json())
          .then(projects => res(projects.reverse()))
      : getMockData().then(projects => res(projects.reverse()))
  })
