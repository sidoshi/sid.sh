import Typography from 'typography'
import kirkhamTheme from 'typography-theme-kirkham'

import theme from './theme'

const googleFonts = kirkhamTheme.googleFonts.map(
  font =>
    font.name === 'Playfair Display'
      ? {
          ...font,
          styles: ['400', '400i', '700', '700i'],
        }
      : font
)

const typographyTheme = {
  ...kirkhamTheme,
  googleFonts,
  headerColor: theme.primaryText,
  bodyColor: theme.primaryText,
  overrideThemeStyles: ({ rhythm }, options, styles) => ({
    '*': {
      boxSizing: 'border-box',
    },

    body: {
      backgroundColor: theme.background,
    },

    h1: {
      margin: '40px 0',
    },
    'h2, h3': {
      margin: '30px 0',
    },
    p: {
      marginBottom: '0.8rem',
      marginTop: '0.8rem',
    },

    a: {
      textDecoration: 'none',
      color: '#1794a5',
      transition: '0.2s',
    },
    'a:hover': {
      color: '#f1404b',
    },
    'a.active': {
      borderBottom: '1px solid',
      color: '#f1404b',
    },
  }),
}

const typography = new Typography(typographyTheme)

export default typography
