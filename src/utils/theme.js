const primary = '#1794A5'
const secondary = '#F1404B'

const theme = {
  primaryText: '#444',
  secondaryText: '#888',
  primaryLink: primary,
  secondaryLink: secondary,
  background: '#fafafa',

  primary,
  secondary,
}

module.exports = theme
