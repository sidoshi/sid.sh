import React from 'react'

import TelegramIcon from 'react-icons/lib/fa/paper-plane'
import TwitterIcon from 'react-icons/lib/fa/twitter'
import GithubIcon from 'react-icons/lib/fa/github'
import GitlabIcon from 'react-icons/lib/fa/gitlab'
import StackOverflowIcon from 'react-icons/lib/fa/stack-overflow'

// Render function
const r = s => {
  const format = st =>
    st
      .replace(/\n/g, ' ')
      .replace(/\s\s+/g, ' ')
      .trim()

  return Array.isArray(s) ? s.map(format).join('\n') : format(s)
}

export default {
  name: 'Siddharth Doshi',
  bio: r(
    `I am a proficient Software Developer with strong affection towards
    JavaScript. I build powerful and maintainable Backend Services and Browser
    applications.`
  ),
  url: 'https://sid.sh',

  platforms: [
    {
      url: 'https://github.com/sidoshi',
      component: <GithubIcon />,
      title: 'Github',
    },
    {
      url: 'https://gitlab.com/sidoshi',
      component: <GitlabIcon />,
      title: 'Gitlab',
    },
    {
      url: 'https://twitter.com/_sidoshi',
      component: <TwitterIcon />,
      title: 'Twitter',
    },
    {
      url: 'https://stackoverflow.com/users/7213112/sidoshi',
      component: <StackOverflowIcon />,
      title: 'Stack Overflow',
    },
    {
      url: 'https://t.me/sidoshi',
      component: <TelegramIcon />,
      title: 'Telegram',
    },
  ],
}
