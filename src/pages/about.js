import React from 'react'
import styled from 'react-emotion'
import { OutboundLink } from 'gatsby-plugin-google-analytics'

import HeaderAndMetaTags from '../components/HeaderAndMetaTags'
import WidthWrapper from '../components/WidthWrapper'

export default () => (
  <WidthWrapper>
    <HeaderAndMetaTags title="About & Contact" url="https://sid.sh/about" />
    <article>
      <h1>About Me</h1>

      <h3>Who & Where</h3>
      <p>
        Hello friend 👋, I am Siddharth Doshi, a Computer Programmer 👨‍💻. I spend
        most of my time learning new things and building cool stuff. I like to
        reason about things and I enjoy solving problems with the skills that I
        gain in the process. Most of the time, I work on Web-related areas
        building awesome Services and SPAs.
      </p>

      <h3>Work</h3>
      <p>
        I worked at SocialPilot as a Senior Javascript Developer where I helped
        migrate a PHP monolith to NodeJs microservices. I currently work as
        remote developer taking independent contracts.
      </p>

      <h3>Approach Towards Programming</h3>
      <p>
        I think of programming as more of a process than skill. The process of
        learning new things and using that knowledge to solve problems. Learning
        new things quickly is just as important part of programming as writing
        code. I take maintainability and readability of the codebase very
        seriously.
      </p>

      <h3>Current Interests</h3>
      <p>
        I am completely dedicated to software development. I have inarticulate
        ambitions and I believe software development is an avenue towards
        achieving them. I am driven by the idea of someday having an immense
        impact on the world.
      </p>
      <p>
        While I have a major interest in developing scalable services, I also
        enjoy Frontend development and I am good with the React ecosystem. I am
        also interested in financial markets and cryptocurrencies. Occasionally,
        I like playing snooker in my spare time.
      </p>

      <h3>Tool Stack</h3>
      <p>
        I use Git as my VCS and Github or Gitlab for hosting repositories. I use
        VScode for almost all programming environments. I am decently familiar
        with Javascript and the ecosystem around it. So I try to use it
        everywhere I can to stay productive. On the backend, I use Express or
        Koa with Node for developing REST APIs. On the frontend, I use the
        React/Redux stack most of the time.
      </p>
    </article>

    <article id="contact">
      <h1>Let's Get In Touch</h1>

      <p>
        You can email me at{' '}
        <OutboundLink href="mailto:mail@sid.sh">mail@sid.sh</OutboundLink>.
        <br />
        I also reply to messages on{' '}
        <OutboundLink href="https://t.me/sidoshi">
          Telegram
        </OutboundLink> or{' '}
        <OutboundLink href="https://twitter.com/_sidoshi">Twitter</OutboundLink>.
        <br />
      </p>
      <p>I’d be glad to hear from you. 😊</p>
    </article>
  </WidthWrapper>
)
