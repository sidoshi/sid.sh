import React from 'react'

import Bio from '../components/Bio'
import RecentWorks from '../components/RecentWorks'
import RecentBlogs from '../components/RecentBlogs'
import getLatestRepos from '../utils/getLatestRepos'

export default class extends React.Component {
  state = {
    projects: undefined,
  }
  componentDidMount() {
    getLatestRepos().then(projects =>
      this.setState({
        projects,
      })
    )
  }
  render() {
    const blogs = this.props.data.allMarkdownRemark.edges.map(({ node }) => ({
      id: node.id,
      title: node.frontmatter.title,
      date: node.frontmatter.date,
      time: node.timeToRead,
      excerpt: node.excerpt,
      url: '/blog' + node.fields.slug,
    }))

    return (
      <div>
        <Bio />
        <RecentBlogs blogs={blogs} />
        <RecentWorks projects={this.state.projects} />
      </div>
    )
  }
}

export const query = graphql`
  query IndexQuery {
    allMarkdownRemark(
      limit: 5
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
          excerpt
          timeToRead
        }
      }
    }
  }
`
