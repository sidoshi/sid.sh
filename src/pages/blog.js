import React from 'react'
import styled from 'react-emotion'
import Link from 'gatsby-link'

import WidthWrapper from '../components/WidthWrapper'
import BlogsList from '../components/BlogsList'
import HeaderAndMetaTags from '../components/HeaderAndMetaTags'

const Wrapper = styled.div`
  min-height: 80%;
`

export default ({ data }) => {
  const blogs = data.allMarkdownRemark.edges.map(({ node }) => ({
    id: node.id,
    title: node.frontmatter.title,
    date: node.frontmatter.date,
    time: node.timeToRead,
    excerpt: node.excerpt,
    url: '/blog' + node.fields.slug,
  }))

  return (
    <WidthWrapper marginTop="50px">
      <HeaderAndMetaTags title="Blog" url="https://sid.sh/blog" />
      <BlogsList blogs={blogs} />
    </WidthWrapper>
  )
}

export const query = graphql`
  query BlogsQuery {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
          excerpt
          timeToRead
        }
      }
    }
  }
`
