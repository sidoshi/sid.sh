#!/bin/bash

echo "Starting the deployment"
git push origin HEAD
git checkout deploy
git merge master -m "Deploy"
git push origin deploy
git checkout -
echo "Done"